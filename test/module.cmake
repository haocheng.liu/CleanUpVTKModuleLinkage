vtk_module(test
	GROUPS
    StandAlone
  DEPENDS
    vtkParallelCore
    vtkFiltersAMR
  PRIVATE_DEPENDS
    vtkhdf5
    vtksys
  TEST_DEPENDS
    vtkIOXML
    vtkTestingCore
    vtkTestingRendering
  KIT
    vtkParallel  
   
  )
