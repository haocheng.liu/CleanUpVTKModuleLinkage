# Objective: Update the module public and private dependency in VTK
# Author: Haocheng LIU  
# Date: 1st August 2016


#############    include needed module   #############    
from FindModuleDependency import *
######################################################



# @Description: create a map beteen path with module.cmake and its module dependency(public or private. Depend on the input) 
# @input: a list of tuples with(subPath, correspondingPathInSet), mapPathModule
# @output: a map between path and its module dependencyList def CreateMapPathDependency(PathAndSet,MapPathModule):
def CreateMapPathDependency(PathAndSet,MapPathModule):
    resultMap = dict()
    for pathSetTuple in PathAndSet:
        currentPath, currentSet = pathSetTuple[0], pathSetTuple[1]
        if MapPathModule.get(currentPath) == None:continue
        currentPathDependencyList = [] 
        for pathInCurrentSet in currentSet:
            pathMapToModule = MapPathModule.get(pathInCurrentSet)
            # you should check if pathMapToModule will be None.(I think not)
            if pathMapToModule != None: currentPathDependencyList.append(pathMapToModule) 
        resultMap[currentPath] = currentPathDependencyList
    return resultMap

# @Description: find the new dependency and private dependency list given the path
# @input: currentPath, MapPathPublicD, mapPathPrivateD
# @output: two lists contain public and private dependencies
def FindNewDandPDList(CurrentPath, MapPathPublicD,MapPathPrivateD):
    publicDependencyList = MapPathPublicD.get(CurrentPath)
    privateDependencyList = MapPathPrivateD.get(CurrentPath)
    return publicDependencyList, privateDependencyList

# @Description: get the DEPENDS and PRIVATE_DEPENDS info from the module.cmake file and the corresponding head, middle and tail.
# @input: path to the module.cmake file
# @output: two lists containing elements in DEPENDS and PRIVATE_DEPENDS and corresponding head, middle, tail in line format.
def GetDandPDInfo(InputPath=""):
    # read data line by line and strip the '\n'
    with open (os.path.join(InputPath,'module.cmake')) as fin:
        inputData = [x.strip('\n') for x in fin.readlines()] 
    DependList, PDependList = [],[]
    indexDStart,indexDEnd,indexPDStart,indexPDEnd = 0, -1, 0, -1
    spaceNum = 2 # number of space before DEPENDS and PRIVATE_DEPENDS
    for index in xrange( len(inputData)):
        # here is a tricky thing: space before DEPENDS can be problematic. Currently only support 2,4,6
        if inputData[index] == "  DEPENDS" or inputData[index] == "    DEPENDS" or inputData[index] == "      DEPENDS":
            spaceNum = inputData[index].find("D") 
            indexDStart = index
            indexW = index + 1
            while (len(inputData[indexW]) >= (spaceNum + 5) and inputData[indexW][0:(spaceNum + 2)] == " " * (spaceNum+2)): 
                # filter the comment
                if "#" in inputData[indexW]: 
                    pondPosition = inputData[indexW].find("#")
                    moduleWithOutComment = inputData[indexW][0:pondPosition] 
                else: moduleWithOutComment = inputData[indexW]
                DependList.append(moduleWithOutComment.replace(" ",""))
                indexW += 1
            indexDEnd = indexW - 1
            # update indexPDStart and indexPDEnd also
            indexPDStart,indexPDEnd = indexDEnd, indexDEnd
        if inputData[index] == "  PRIVATE_DEPENDS" or inputData[index] == "    PRIVATE_DEPENDS" or inputData[index] == "      PRIVATE_DEPENDS": 
            spaceNum = inputData[index].find("P")
            indexPDStart = index
            indexW = index + 1
            # 5 = 2 spaces + 3 characters as vtk
            while (len(inputData[indexW]) >= (spaceNum + 5) and inputData[indexW][0:(spaceNum + 2)] == " " * (spaceNum+2)): 
                # filter the comment
                if "#" in inputData[indexW]: 
                    pondPosition = inputData[indexW].find("#")
                    moduleWithOutComment = inputData[indexW][0:pondPosition] 
                else: moduleWithOutComment = inputData[indexW]
                PDependList.append(inputData[indexW].replace(" ",""))
                indexW += 1
            indexPDEnd = indexW - 1 
    # handle different cases.
    head,middle,tail = inputData[0:indexDStart], inputData[(indexDEnd+1):(indexPDStart)] ,inputData[(indexPDEnd+1):] 
    # remove empty element
    head = [x for x in head if x]
    middle = [x for x in middle if x]
    tail = [x for x in tail if x]
    return DependList,PDependList,head,middle,tail,spaceNum

# @Description: process the DependList and PDependList from GetDandPDInfo, handling missing cases as header name repetition, third party dependency,legacy code such as vtklibproj4 and wrapping as vtkPython.
# @input: DependList, PDependList from GetDandPDInfo, modules from Utilities and Thirdparties and MustAddList for repetition
# @output: DependList and PDependList processed
# vtkGraphItem.h points to different folder. but since it's self-included, we can ignore it.
def FilterDandPDList(DependList,PDependList,ModulesInUandT,MustAddList):
    # update the filter list in the future if you want to keep anything untouched
    MustAddList.extend(ModulesInUandT)
    DependListFiltered, PDependListFiltered, = [], []
    for item in DependList:
        if item in MustAddList:
            DependListFiltered.append(item)
    for item in PDependList:
        if item in MustAddList:
            PDependListFiltered.append(item)
    return DependListFiltered, PDependListFiltered

# @Description: given a path, find all the module name inside it. Helper function for FilterDandPDList
# @input: path 
# @output: a list containing all the module name inside the path
def GetModuleInPath(Path):
    allPaths = GetAllPath(Path)
    moduleList = []
    for currentPath in allPaths:
        for f in os.listdir(currentPath):
            if os.path.isfile(os.path.join(currentPath,f)) and f == "module.cmake":
                moduleName = GetModuleName(os.path.join(currentPath,f))
                moduleList.append(moduleName)
    return moduleList

# @Description: given a vtk path, get the modules in Utlities and ThirdParty
# @input: path
# @output: a list contains module in Utlities and Thirdparty
def GetModulesInUandT(Path): 
    result = [] 
    # add modules in Utilities and Thirdparty to mustAddList
    result.extend( GetModuleInPath(os.path.join(Path,"Utilities")))
    result.extend( GetModuleInPath(os.path.join(Path,"ThirdParty")))
    return result

# @Descrpition: combine and sort the dependencyList and dependListFiltered, making sure no collision for vtkRenderingEnine helper function for UpdateModuleCMake
# @input: dependencyList and dependListFiltered
# @output: newDlist
def CombineDLandDLFiltered(DependList, DependListFiltered, DependListAddedExtra):
    # make sure that there is no repetition
    DependList.extend(DependListFiltered)
    DependList.extend(DependListAddedExtra)
    answer = list(set(DependList))
    # make sure that vtkOpenGL1&2 do not collide with vtkRendering${VTK_RENDERING_BACKEND}
    if "vtkRendering${VTK_RENDERING_BACKEND}" in answer:
        if "vtkRenderingOpenGL" in answer:
            indexOpenGL = answer.index("vtkRenderingOpenGL")
            answer = answer[0:indexOpenGL] + answer[(1+indexOpenGL):]
        if "vtkRenderingOpenGL2" in answer:
            indexOpenGL2 = answer.index("vtkRenderingOpenGL2")
            answer = answer[0:indexOpenGL2] + answer[(1+indexOpenGL2):] 
    answer.sort()
    return answer
    
# @Description: find out public and private modules that needed be add to current path, helper function for UpdateModuleCMake
# @input: currentPath, addPublicModuleList, addPrivateModuleList
# @output: PublicModuleListAddedExtra, PrivateModuleListAddedExtra
def FindAddedExtraList(CurrentPath,AddPublicModuleList, AddPrivateModuleList):
    publicModuleListAddedExtra, privateModuleListAddedExtra = [], []
    for item in AddPublicModuleList:
        if item[0] == CurrentPath:
            publicModuleListAddedExtra.append(item[1])
    for item in AddPrivateModuleList:
        if item[0] == CurrentPath:
            privateModuleListAddedExtra.append(item[1])
    return publicModuleListAddedExtra, privateModuleListAddedExtra


##################################################################################
######                         main function                                ######
##################################################################################

# @Description: update the module.cmake file in VTK folder. Check the README.md for reference
# @input: path to VTK folder, mustAddList and IgnorePathFileList, excludePublicModuleList,excludePrivateModuleList)
def UpdateModuleCMake(Path, MustAddList, IgnorePathFileList, ExcludePublicModuleList,ExcludePrivateModuleList, AddPublicModuleList, AddPrivateModuleList):
    # get all subpaths that have module.cmake file, loop through this map
    mapPathModule = CreateMapPathModule(Path)
    # get corresponding path for header and source file, making it a map
    pathHeaderPathList, dump = FindCorrespondingPath(Path,"h",IgnorePathFileList)
    pathSourcePathList, dump = FindCorrespondingPath(Path,".cxx",IgnorePathFileList)
    mapPathPublicD = CreateMapPathDependency(pathHeaderPathList,mapPathModule)
    mapPathPrivateD = CreateMapPathDependency(pathSourcePathList,mapPathModule)
    # get the modules in Utlities and Thirdparty
    modulesInUandT = GetModulesInUandT(Path)
    # loop through each path and update the module.cmake file
    for currentPath in mapPathModule:
        # get the module added as extra in currentPath
        publicModuleListAddedExtra, privateModuleListAddedExtra = FindAddedExtraList(currentPath,AddPublicModuleList, AddPrivateModuleList)
        # get the current publicDependency
        publicDependList,privateDependList,head,middle,tail,spaceNum = GetDandPDInfo(currentPath)
        # filter publicDependency and private list        
        # it might be empty. Be careful with the empty case
        publicDependListFiltered, privateDependListFiltered = FilterDandPDList(publicDependList, privateDependList, modulesInUandT, MustAddList)
        publicDependencyList, privateDependencyList = FindNewDandPDList(currentPath,mapPathPublicD,mapPathPrivateD)
        # combine newPublicDList,newPrivateDList,head,middle,tail 
        # combine order: head, middle,  tail[0:-1], newPublicDList, newPrivateDList, tail[-1](which is "  )") 
        newPublicDList = CombineDLandDLFiltered(publicDependencyList, publicDependListFiltered,publicModuleListAddedExtra) 
        newPrivateDList = CombineDLandDLFiltered(privateDependencyList, privateDependListFiltered,privateModuleListAddedExtra) 
        # make sure repeated module is not included
        newPrivateDList = [item for item in newPrivateDList if item not in newPublicDList]
        # check the value of endIndex of tail. Special case is vtk/Thirdparty/hdf5
        # make sure that there is no blank line at the end!!!!!
        checkingIndex = -1
        while tail[checkingIndex] == "": checkingIndex -= 1

        if tail[checkingIndex] == "endif()" or tail[checkingIndex] == "endif ()": endIndex = -2
        else: endIndex = -1 
        # For debug usage
        if currentPath == "the path you want to debug": 
            print "Dependency before change is:\n",publicDependList,"\n",privateDependList
            print "Dependecy before change filtered is:\n",publicDependListFiltered,"\n",privateDependListFiltered
            print "Dependency after change is:\n",publicDependencyList,"\n",privateDependencyList
            print "head is:\n",head,"\n\nmiddle is:\n",middle,"\n\ntail is:\n",tail
            print "Final public list is:\n",newPublicDList, "\n\nFinal private list is:",newPrivateDList
            print "endIndex is:",endIndex
        content = ""
        if head != []:
            content += ("\n").join(head) + "\n" 
        if middle != []:
            content += ("\n").join(middle) + "\n"
        if len(tail) > 1:
            content += ("\n").join(tail[0:endIndex]) + "\n"
        if len(newPublicDList) != 0:
            content += " "*spaceNum + "DEPENDS\n"
            for item in newPublicDList:
                if (currentPath,item) not in ExcludePublicModuleList:
                    content += " "*spaceNum + "  %s\n"%(item)
        if len(newPrivateDList) != 0:
            content += " "*spaceNum + "PRIVATE_DEPENDS\n"
            for item in newPrivateDList:
                if (currentPath,item) not in ExcludePrivateModuleList:
                    content += " "*spaceNum + "  %s\n"%(item)
        if endIndex == -1: content += tail[-1]
        else: content += tail[-2] + "\n"+ tail[-1]
        # write content into module.cmake
        writeFile(os.path.join(currentPath,"module.cmake"),content) 

if __name__ == "__main__":
    path = "/home/haochengliu/Kitware/VTK/vtk"
    mustAddList = ["vtkRendering${VTK_RENDERING_BACKEND}", 
                    "vtkRenderingOpenGL",
                    "vtkRenderingOpenGL2",
                    "vtkRenderingGL2PSOpenGL2",
                    "${VTK_RENDERINGVOLUMEOPENGLNEW}",
                    "vtkRenderingContext${VTK_RENDERING_BACKEND}",
                    "vtkRenderingVolume${VTK_RENDERING_BACKEND}",
                    "vtklibproj4",
                    "${gl2ps_depends}",
                    "${vtkIOMovie_vtkoggtheora}",
                    "${__priv_deps}"]
    ignorePathFileList = [("vtk/Rendering/OpenGL","vtkXRenderWindowTclInteractor.cxx")]
    # modules you want to exclude from certain path. [(path,moduleName)]
    excludePublicModuleList = [("vtk/GUISupport/Qt","vtkRenderingOpenGL"),("vtk/Rendering/ParallelLIC","vtkRenderingLIC"),("vtk/Rendering/ParallelLIC","vtkRenderingLICOpenGL2"),("vtk/Filters/Core","vtkFiltersPoints")]
    excludePrivateModuleList = [("vtk/Rendering/OpenGL2","vtkRenderingOpenGL"),("vtk/Views/Qt","vtkGUISupportQt"),("vtk/Views/Qt","vtkViewsInfovis"),("vtk/Views/Infovis","vtkRenderingQt"),("vtk/Infovis/Layout","vtkInfovisBoostGraphAlgorithms"),("vtk/Filters/Core","vtkFiltersPoints"),("vtk/Rendering/Volume","vtkIOImage"),("vtk/IO/Geometry","vtkRenderingCore")]
    # modules you want to add to certain path. [(path, moduleName)]
    addPublicModuleList = [("vtk/Views/Qt","vtkGUISupportQt"),("vtk/Views/Qt","vtkViewsInfovis"),("vtk/Rendering/Label","vtkRenderingFreeType"),("vtk/Views/Core","vtkInteractionWidgets")] 
    addPrivateModuleList = [("vtk/Rendering/Qt","vtkGUISupportQt"),("vtk/IO/SQL","vtksqlite")]  
    UpdateModuleCMake(path, mustAddList, ignorePathFileList, excludePublicModuleList,excludePrivateModuleList, addPublicModuleList, addPrivateModuleList)

