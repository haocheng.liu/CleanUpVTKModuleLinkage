# Objective: given a path A, find out all the paths for the headers that the .alpha file inside path A include.(.alpha can be .cxx or .h file)
# Author: Haocheng LIU
# Date: July 27th 2016

import os, sys,re

def readFile(filename, mode="rt"):
    # rt = "read text"
    with open(filename, mode) as fin:
        return fin.read()

def writeFile(filename, contents, mode="wt"):
    # wt = "write text"
    with open(filename, mode) as fout:
        fout.write(contents)

# @Description: get all the paths within the given path
# @Input: path 
# @Output: a list of all the paths
def GetAllPath(InputPath):
    answer = []
    for root, subdirs, files in os.walk(InputPath):
    	answer.append(root)
    return answer

# @Description: know the .alpha file in a path
# @Input: path and the suffix of the file Ex. ".h", ".cxx"
# @Outpu: a list of all the files in current path that ends with suffix
def GetDotSuffixFileInPath(InputPath, Suffix):
    result = []
    suffixLength = len(Suffix)
    for f in os.listdir(InputPath):
        if os.path.isfile(os.path.join(InputPath,f)) and f[-suffixLength:] == Suffix:
            result.append(f)
    return result

# @Description: extract the included header file inside the .alpha file
# @input: filename you want chec and the path
# @outpu: a list of all the headers included in "" or <> format
def GetHeaderFileFromAlphaFile(InputFilename,Path=""):
    inputData = readFile(os.path.join(Path,InputFilename))
    pattern = r'(#include )\W(vtk.*\.h)\W'
    match = re.findall(pattern,inputData)
    answer = []
    for item in match:
        answer.append(item[1])
    return answer

# @Description: create a map between the header and path
# @input: the 1st level path
# @output: a map between all the headers and their corresponding path
def CreateMapHeaderPath(InputPath):
    result = dict()
    allPath = GetAllPath(InputPath)
    for currentPath in allPath:
        headerFileList = GetDotSuffixFileInPath(currentPath,".h")
        for currentHeaderFile in headerFileList:
            result[currentHeaderFile] = currentPath
    return result

# @Description: find the relation between the given path and file with specific file
# @input: path and the suffix of the file EX. ".h", ".cxx"
# @output: a list of tuples with (subpath, correspondingPathInSet)
def FindCorrespondingPath(Path, Suffix):
    # get all sub paths and create a map between head and map
    allSubPaths = GetAllPath(Path)
    mapHeaderPath = CreateMapHeaderPath(Path)
    filesWithOutPath = []
    result = []
    # before is good
    for currentSubPath in allSubPaths:
            # get all the file end in suffix 
            currentSubPathSet = set()
            files = GetDotSuffixFileInPath(currentSubPath, Suffix)
            for currentFile in files:
                # find out it includes which header
                headerFilesIncluded = GetHeaderFileFromAlphaFile(currentFile,currentSubPath)
                currentFilesWithOutpath = []
                for currentHeaderFileIncluded in headerFilesIncluded:
                    
                    correspondingPath = mapHeaderPath.get(currentHeaderFileIncluded)
                    if correspondingPath == None: 
                        # record file without Path. ideally it should only be .*module.h
                        currentFilesWithOutpath.append(currentHeaderFileIncluded)
                    else:
                        # add the headerPath to the currentSubPathSet
                        currentSubPathSet.add(correspondingPath)
                filesWithOutPath.append( (currentFilesWithOutpath,currentFile) )
            result.append ( (currentSubPath, currentSubPathSet) )
    return result,filesWithOutPath

# @Description: write the output into file
# @input: result,filesWithOutPath from findCorrespondingPath, desire filename
# @output: file
def WriteResult(result,filesWithOutPath,suffix,filename = "result.txt"):
    answer = ""
    # write the paths and their corresponding included path
    answer += "Result of searching with files ending in %s\n\n"%(suffix)
    answer += "Example:\nHave file Common/Core/A.h and Common/ExecuteModel/B.h\nKnows that IO/Geometry/C.h which includes A.h and IO/Geometry/D.h which includes (A.h B.h)\nThen we have follwing dependency result:\nIO/Geometry depends on folder:\n\tCommon/Core\n\tCommon/ExecuteModel\n\n"
    for resultTuple in result:
        answer += "%s\n"%(resultTuple[0])
        if len(resultTuple[1]) == 0:
            answer += "\tno file ended with %s\n"%(suffix)
        for currentPath in resultTuple[1]:
            answer += "\t%s\n"%(currentPath) 
    answer += "\nSome headerfiles do not have path in VTK, possibly are .*module.h files.\n"
    for resultTuple in filesWithOutPath:
        if len(resultTuple[0]) == 0: continue
        answer += "In file %s, the following headers do not have a path in VTK:\n"%(resultTuple[1])
        for currentHeader in resultTuple[0]:
            answer += "\t%s\n"%(currentHeader)
    writeFile(filename,answer)
    return answer         

def main(path,suffix,filename):  
    result,filesWithOutPath =  FindCorrespondingPath(path,suffix)
    WriteResult(result, filesWithOutPath,suffix,filename)


if __name__ == '__main__':
    if len(sys.argv) != 4:
        sys.stdout.write("Check your input again.\nDesire input format: path suffix filename\n")
    else: 
        path, suffix, filename = sys.argv[1],sys.argv[2],sys.argv[3]
        main(path,suffix,filename)
