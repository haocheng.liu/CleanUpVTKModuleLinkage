#############################################################################
# Objective: Given a path to module.cmake file, update the private and public
# dependency within it based on **VTK's module information**. Useful for debug
# and creating new module.

# Author: Haocheng LIU
# Date: Sep 12nd 2016
##############################################################################

from UpdateModuleDependency import *
from FindModuleDependency import *


# @Description: given a path and suffix, find out files within the path with this suffix includes what vtk headers.
# @input: path you want to check and suffix of the file. Ex(.cxx, .C, .h)
# @ouput: a set of header inclusion
def FindHeaderInclusionOfSuffix(Path,Suffix):
    allPaths = GetAllPath(Path)
    result = set()
    for currentSubPath in allPaths:
        files = GetDotSuffixFileInPath(currentSubPath, Suffix)
        for currentFile in files:
            headFilesIncluded = GetHeaderFileFromAlphaFile(currentFile,currentSubPath)
            result.update(headFilesIncluded)
    return result


# @Convert headers into module
# @input: headerInclusion, sourceInclusion, mapHeaderPath and mapPathModule
# @output:
def GetPandPDModule(HeaderInclusion, SourceInclusion, MapHeaderPath,MapPathModule):
    publicDependListMapped,privateDependListMapped = [], []
    currentResult = None
    for item in HeaderInclusion:
        if MapHeaderPath.get(item):
            currentResult =MapPathModule.get(MapHeaderPath.get(item)[0])
        if currentResult != None:
            publicDependListMapped.append(currentResult)
    for item in SourceInclusion:
        if MapHeaderPath.get(item):
            currentResult =MapPathModule.get(MapHeaderPath.get(item)[0])
        if currentResult != None:
            privateDependListMapped.append(currentResult)
    publicDependListMapped = list(set(publicDependListMapped))
    privateDependListMapped = list(set(privateDependListMapped))
    privateDependListMapped = [item for item in privateDependListMapped if item not in publicDependListMapped]
    return publicDependListMapped,privateDependListMapped



def UpdateSingleModuelCMake(VtkPath,TargetModuleCMakeFilePath,MustAddList):
    # create a map[Header] = PATH
    mapHeaderPath = CreateMapHeaderPath(VtkPath)
    # create a map[PATH] = module
    mapPathModule = CreateMapPathModule(VtkPath)
    # get the modules in Utilities and Thirdpary. Add them to mustAddList
    modulesInUandT = GetModulesInUandT(VtkPath)
    ###########################################################################
    #####  add new suffix for header and source files in the directory    #####
    ###########################################################################
    headerInclusion = FindHeaderInclusionOfSuffix(TargetModuleCMakeFilePath,"h")
    sourceInclusion = FindHeaderInclusionOfSuffix(TargetModuleCMakeFilePath,"C")
    sourceInclusion.update(FindHeaderInclusionOfSuffix(TargetModuleCMakeFilePath,".cxx"))
    # map these header into module
    publicDependListMapped,privateDependListMapped = GetPandPDModule(headerInclusion, sourceInclusion, mapHeaderPath, mapPathModule)
    # get the current publicDependency
    publicDependList,privateDependList,head,middle,tail,spaceNum = GetDandPDInfo(TargetModuleCMakeFilePath)
    # filter publicDependency and private list from original module.cmake       
    # it might be empty. Be careful with the empty case
    publicDependListFiltered, privateDependListFiltered = FilterDandPDList(publicDependList, privateDependList, modulesInUandT, MustAddList)    
    newPublicDList = CombineDLandDLFiltered(publicDependListMapped, publicDependListFiltered,[]) 
    newPrivateDList = CombineDLandDLFiltered(privateDependListMapped, privateDependListFiltered,[]) 

    # write result
    checkingIndex = -1
    while tail[checkingIndex] == "": checkingIndex -= 1
    if tail[checkingIndex] == "endif()" or tail[checkingIndex] == "endif ()": endIndex = -2
    else: endIndex = -1
    content = ""
    if head != []:
        content += ("\n").join(head) + "\n" 
    if middle != []:
        content += ("\n").join(middle) + "\n"
    if len(tail) > 1:
        content += ("\n").join(tail[0:endIndex]) + "\n"
    if len(newPublicDList) != 0:
        content += " "*spaceNum + "DEPENDS\n"
        for item in newPublicDList:
            content += " "*spaceNum + "  %s\n"%(item)
    if len(newPrivateDList) != 0:
        content += " "*spaceNum + "PRIVATE_DEPENDS\n"
        for item in newPrivateDList:
            content += " "*spaceNum + "  %s\n"%(item)
    if endIndex == -1: content += tail[-1]
    else: content += tail[-2] + "\n"+ tail[-1]
    # write content into module.cmake
    print "content is:\n",content
    writeFile(os.path.join(TargetModuleCMakeFilePath,"module.cmake"),content) 





if __name__ == "__main__":
    print "Use this script to update moduel.cmake file based on VTK module definition."
    print "Before use, set your vtk-path and target-module.cmake-file first."
    print "The order in extreme case might need modification(wrong bracket place). Corect it manually if needed."
    print "Warning: Do not use it to test VTK module! VTK module depends on file within same directory, but this script would search recursively, so test dependencies would be wrongly added."
    vtkPath = "vtk"
    targetModuleCMakeFilePath = "/home/haochengliu/Desktop/operators"
    mustAddList = ["vtkRendering${VTK_RENDERING_BACKEND}", 
                    "vtkRenderingOpenGL",
                    "vtkRenderingOpenGL2",
                    "vtkRenderingGL2PSOpenGL2",
                    "${VTK_RENDERINGVOLUMEOPENGLNEW}",
                    "vtkRenderingContext${VTK_RENDERING_BACKEND}",
                    "vtkRenderingVolume${VTK_RENDERING_BACKEND}",
                    "vtklibproj4",
                    "${gl2ps_depends}",
                    "${vtkIOMovie_vtkoggtheora}",
                    "${__priv_deps}"]
    UpdateSingleModuelCMake(vtkPath,targetModuleCMakeFilePath,mustAddList)
   
