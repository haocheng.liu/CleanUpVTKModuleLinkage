# Objective: given a path A, find out all the paths for the headers that the .alpha file inside path A include.(.alpha can be .cxx or .h file), then replace the file with the corresponding module name in module.cmake file.
# Author: Haocheng LIU
# Date: July 27th 2016

import os, sys,re

def readFile(filename, mode="rt"):
    # rt = "read text"
    with open(filename, mode) as fin:
        return fin.read()

def writeFile(filename, contents, mode="wt"):
    # wt = "write text"
    with open(filename, mode) as fout:
        fout.write(contents)

# @Description: get all the paths within the given path
# @Input: path 
# @Output: a list of all the paths
def GetAllPath(InputPath):
    answer = []
    for root, subdirs, files in os.walk(InputPath):
        answer.append(root)
    return answer

# @Description: know the .alpha file in a path
# @Input: path and the suffix of the file Ex. ".h", ".cxx"
# @Outpu: a list of all the files in current path that ends with suffix
def GetDotSuffixFileInPath(InputPath, Suffix):
    result = []
    suffixLength = len(Suffix)
    for f in os.listdir(InputPath):
        if os.path.isfile(os.path.join(InputPath,f)) and f[-suffixLength:] == Suffix:
            result.append(f)
    return result

# @Description: extract the included vtk header file inside the .alpha file
# @input: filename you want to check and the path
# @outpu: a list of all the headers included in "" or <> format
def GetHeaderFileFromAlphaFile(InputFilename,Path=""):
    inputData = readFile(os.path.join(Path,InputFilename))
    pattern = r'(#include )\W(vtk.*\.h)\W'
    match = re.findall(pattern,inputData)
    answer = []
    for item in match:
        answer.append(item[1])
    return answer

# @Description: create a map between the header and path
# @input: the 1st level path
# @output: a map between all the headers and their corresponding path
def CreateMapHeaderPath(InputPath):
    result = dict()
    allPath = GetAllPath(InputPath)
    for currentPath in allPath:
        headerFileList = GetDotSuffixFileInPath(currentPath,".h")
        for currentHeaderFile in headerFileList:
            # current headerFile has been seen before
            if result.get(currentHeaderFile) != None and currentHeaderFile[0:3] == "vtk":
                # mostly are openGL related stuff. Handled by using mustAddList
                # # use a list to store different paths for the currentHeaderFile
                result[currentHeaderFile].append(currentPath)
            elif currentHeaderFile[0:3] == "vtk":
                result[currentHeaderFile] = [currentPath]
    return result

# @Description: given path and file with specific suffix, find the path dependency. Only cares about vtk headers
# @input: path ,the suffix of the file EX. ".h", ".cxx" and a list contains tuple as(path, filename)that you want to ignore
# @output: a list of tuples with (subpath, correspondingPathInSet)
def FindCorrespondingPath(Path, Suffix,IgnorePathFileList):
    # get all subpaths and create a map between head and map
    allPaths = GetAllPath(Path)
    mapHeaderPath = CreateMapHeaderPath(Path)
    filesWithoutPath = []
    result = []
    for currentSubPath in allPaths:
            # get all the file end in suffix 
            currentSubPathSet = set()
            files = GetDotSuffixFileInPath(currentSubPath, Suffix)
            for currentFile in files:
                # check whether the file is in the ignore list
                if (currentSubPath, currentFile) in IgnorePathFileList: 
                    continue
                # find out it includes which header
                headerFilesIncluded = GetHeaderFileFromAlphaFile(currentFile,currentSubPath)
                currentFilesWithoutpath = []
                for currentHeaderFileIncluded in headerFilesIncluded: 
                    correspondingPath = mapHeaderPath.get(currentHeaderFileIncluded)
                    if correspondingPath == None: 
                        # record file without Path. ideally it should only be .*module.h
                        currentFilesWithoutpath.append(currentHeaderFileIncluded)
                    elif len(correspondingPath) == 1:
                        # add the headerPath to the currentSubPathSet
                        # Problem! How do you know this head file belongs to which path? 
                        # currently we only add header without multiple directories
                        # avoid adding self
                        if correspondingPath[0] != currentSubPath:
                            currentSubPathSet.add(correspondingPath[0])
                            #if currentSubPath == 'vtk/Rendering/OpenGL2' and correspondingPath[0] == 'vtk/Rendering/OpenGL':
                            #    print 'header file and resulted path is:'
                            #     print currentHeaderFileIncluded,correspondingPath[0]
                    # else:
                    #     print "current header file is:", currentHeaderFileIncluded
                    #     print "\tcorrespondingPath is: ", correspondingPath
                filesWithoutPath.append( (currentFile, currentFilesWithoutpath) )
            result.append ( (currentSubPath, currentSubPathSet) )
    return result,filesWithoutPath

# @Description: write the output into file
# @input: result,filesWithoutPath from findCorrespondingPath, desire filename
# @output: file
def WriteResult(path,result,filesWithoutPath,suffix,filename = "result.txt"):
    writeHeaderFilesWithoutPathFlag = 0 # flag to control whether write out header files without path
    answer = ""
    mapPathModule = CreateMapPathModule(path)
    # write the paths and their corresponding included path
    answer += "Example:\nHave file Common/Core/A.h and Common/ExecuteModel/B.h\nKnows that IO/Geometry/C.h which includes (A.h) and IO/Geometry/D.h which includes (A.h B.h)\nThen we have follwing dependency result:\nIO/Geometry depends on folder:\n\tCommon/Core\n\tCommon/ExecuteModel\n\n"
    answer += "In module form, it's:\nvtkIOGeometry:\n\tvtkCommonCore\n\tvtkCommonExecutionModel\n"
    answer += "\n\n\nResult of searching with files ending in %s\n\n"%(suffix)
    for pathSetTuple in result:
        # skip the path without module.cmake
        if mapPathModule.get(pathSetTuple[0]) == None:
            continue
        # output on path with module.cmake
        answer += "%s\n"%(mapPathModule.get(pathSetTuple[0]))
        if len(pathSetTuple[1]) == 0:
            answer += "\tfiles end in %s do not depend on VTK module\n"%(suffix)
        for currentPath in pathSetTuple[1]:
            # answer should not be none because we only handle cases with module
            answer += "\t%s\n"%(mapPathModule.get(currentPath)) 
    if writeHeaderFilesWithoutPathFlag == 1:
        answer += "\nSome headerfiles do not have path in VTK, possibly are .*module.h files.\n"
        for pathSetTuple in filesWithoutPath:
            if len(pathSetTuple[1]) == 0: continue
            answer += "In file %s, the following headers do not have a path in VTK:\n"%(pathSetTuple[0])
            for currentHeader in pathSetTuple[1]:
                answer += "\t%s\n"%(currentHeader)
    writeFile(filename,answer)
    return answer         

# @Description: create a map between the path and module name in module.make
# @input: path 
# @output: a map between path and their module name. If there is no module.cmake, map it with none. 
def CreateMapPathModule(path):
    allPaths = GetAllPath(path) 
    mapPathModule = dict()
    # loop through all path
    for currentPath in allPaths:
       # check whether you have module.cmake
       for f in os.listdir(currentPath):
            # if you have module.cmake, get the module name and pass it into map
            if os.path.isfile(os.path.join(currentPath,f)) and f == "module.cmake":
                # get the module name
                moduleName = GetModuleName(os.path.join(currentPath,f))
                mapPathModule[currentPath] = moduleName
    return mapPathModule

# @Description: get the module name from the module.cmake file
# @input: file path
# @output: module name
def GetModuleName(filepath):
    content = readFile(filepath)
    pattern = r'(vtk_module\()([\w]+)'
    match = re.search(pattern,content)
    return match.group(2)


def main(path,suffix,filename):  
    result,filesWithoutPath =  FindCorrespondingPath(path,suffix)
    WriteResult(path, result, filesWithoutPath,suffix,filename)


if __name__ == '__main__':
    filename = "module_dependency_result_head.txt"
    path = "vtk"
    suffix = ".cxx"
    ignorePathFileList = [("vtk/Rendering/OpenGL","vtkXRenderWindowTclInteractor.cxx")]
    FindCorrespondingPath(path, suffix,ignorePathFileList)