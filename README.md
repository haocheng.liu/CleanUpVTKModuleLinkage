# Detect module dependency for any VTK based project
> - A new script called `UpdpateSingleModuelCMake.py` is added to detect module dependency for any VTK based project.
> - Usage:
>     + specify **vtkPath** and **targetModuleCmakeFilePath** in the script and run it.
>     + Ex: detect visualBridge dependency in ParaView.
>     + Ex: detect what vtkmodule you should link for a folder with header and source files
>       + first create a dummy module.cmake file as:
```
vtk_module(dummyModule
  DEPENDS
  PRIVATE_DEPENDS
  TEST_DEPENDS
)

```
>       + run script. Bravo!
> - Caution:
>     + Do not use this script to test VTK module! Since VTK modules depends on file within the same directory, but this script would search recursively. Use `UpdateModuleDependency.py` instead.


# Clean up the VTK module linkage and Update corresonding ParaView file
> - *Objective*:
>     + This project's objective is to write a script that could automatically clean up the linkage in module.cmake files in vtk folder and document the corresponding changes in ParaView
>       * function `UpdateModuleCmake` in UpdateModuleDependency.py fulfills this task
> - *Usage*:
>     + Download `UpdateModuleDependency.py` and `FindModuleDependency.py` file(helper functions for `UpdateModuleDependency.py`), change VTK_PATH in `UpdateModuleDependency.py` __main__, then run the script.
> - Main idea:
>     + Check the pseudocode in UpdateModuleDependency.py  which is dicussed in the bottom.

## Strategies and rules 
> - Indentation in `module.cmake`: 2 spaces
> - Tail line in module.cmake file: 
>     + `\s+)`, `\s+endif()` or `\s+endif ()`(Before them should be `\s+)`) are acceptable as tail line. The script would take care of these three cases. (Currently these three cover all  cases in VTK)
> - If a module is both needed by public and private, just include it in public_dependency // python script would automatically take care of it
> - Filter the old dependency:
>     + Some modules have header files with same name, especially for OpenGL and OpenGL2 related stuff. This would be annoying since I am using headers to locate the module name. To solve it, I just follow the old convention in VTK to avoid creating new problems. These modules are stored in **mustAddList** which would be discussed in the following part.
>     + Modules in Utilties and ThirdParty have unique naming conventions. So they would be appended into **mustAddList** by the script
>         * Normal rule: `vtk/Rendering/Core` -> module `vtkRenderingCore`
>         * Special rule: `vtk/Thirdparty/AutoBahnPython` - > module `AutoBahnPython`
>     + **mustAddList** 
>         * Module in mustAddList would force the script to preserve these modules in old public and private dependencys
>         * You can just add new module names that you want to preserve in format: [moduleName]
> - current mustAddList is as below:

```text
mustAddList = ["vtkRendering${VTK_RENDERING_BACKEND}", 
                    "vtkRenderingOpenGL",
                    "vtkRenderingOpenGL2",
                    "vtkRenderingGL2PSOpenGL2",
                    "${VTK_RENDERINGVOLUMEOPENGLNEW}",
                    "vtkRenderingContext${VTK_RENDERING_BACKEND}",
                    "vtkRenderingVolume${VTK_RENDERING_BACKEND}",
                    "vtklibproj4",
                    "${gl2ps_depends}",
                    "${vtkIOMovie_vtkoggtheora}",
                    "${__priv_deps}"]
```

> - Corner cases:
> - compile time & run time
>     + The logic that if this module includes a header A in its header file, I would add header A's module into DEPENDS. Referring to source file inclusion, I would add it to PRIVAGE_DEPENDS. `vtkRenderingTk` module includes headers from `vtkWrappingTcl`, so it fits my logic that `vtkRendingTk` depends on `vtkWrappingTcl`. But I did not see that reason why it should work in the reverse order. After a second thought, I guess it's because after I updated all the module dependency, at running time `vtkWrappingTcl` cannot link to `RenderingTk`. But it should be the case because at the running time you need the library rather than the headers. That explains why this patch work. VTK !1965.
>     + **ignorePathFileList** 
>         * format: [(path, filename)]
>         * A list store the file you want the script to ignore. None of the included headers in this file would be taken into consideration
>         * Ex: `vtkXRenderWindowTclInteractor.cxx` in `vtk/Rendering/OpenGL` should be ignored otherwise module vtkWrappingTcl would be added to private_dependency, causing building errors when tcl is not enabled

```text
ignorePathFileList = [("vtk/Rendering/OpenGL","vtkXRenderWindowTclInteractor.cxx")]
```

>     + **excludePublicModuleList/excludePrivateModuleList**
>         * format: [(path,moduleName)]
>         * A list of modules you want to exclude from certain path
>         * Ex: `vtkWin32RenderWindowInteractor.cxx` includes file "vtkTDxWinDevice.h", which belongs to Rendering/OpenGL. Then `Rendering/OpenGL2`  would wrongly include `RenderingOpenGL`, however you can not build two rendering engines at the same time in VTK. This special  case should be added as: **("vtk/Rendering/OpenGL2","vtkRenderingOpenGL")**

```text
excludePublicModuleList = [("vtk/GUISupport/Qt","vtkRenderingOpenGL"),("vtk/Rendering/ParallelLIC    ","vtkRenderingLIC"),("vtk/Rendering/ParallelLIC","vtkRenderingLICOpenGL2"),("vtk/Filters/Core","vtkF    iltersPoints")]
excludePrivateModuleList = [("vtk/Rendering/OpenGL2","vtkRenderingOpenGL"),("vtk/Views/Qt","vtkGU    ISupportQt"),("vtk/Views/Qt","vtkViewsInfovis"),("vtk/Views/Infovis","vtkRenderingQt"),("vtk/Infovis/    Layout","vtkInfovisBoostGraphAlgorithms"),("vtk/Filters/Core","vtkFiltersPoints"),("vtk/Rendering/Vol    ume","vtkIOImage"),("vtk/IO/Geometry","vtkRenderingCore")]

```

>     + addPublicModuleList/addPrivateModuleList 
>         * format: [(path, moduleName)]
>         * A list of modules you want to add to certain path
>         * Ex: add `vtkGUISupportQt` to `vtkRenderingQt` private dependency, or you  would have compile errors

```text
addPublicModuleList = [("vtk/Views/Qt","vtkGUISupportQt"),("vtk/Views/Qt","vtkViewsInfovis"),("vtk/Rendering/Label","vtkRenderingFreeType")] 
addPrivateModuleList = [("vtk/Rendering/Qt","vtkGUISupportQt"),("vtk/IO/SQL","vtksqlite")]  
```

## Dirty data log
> Just for log purpose. No need to update them before and after running the script in the future, only needed in the first time usage. Module addition and removal is taken care of by the script.
> dirty data before using this script in the first time (VTK SHA points to **15da362** before the initial update)
> - indentation rule:
>   + 2 tabs
>   + modules with corrupted indentation
>       * GUISupport/MFC
>       * IO/FFMPEG
>       * IO/ParallelLSDyna
>       * Testing/IOSQL
>       * ThirdParty/VPIC
>       * Common/System
> - wrong order
>     + last line of the file should be `\s+endif()` or `\s+endif ()` Move the if statement up.
>       * Thirdparty/hdf5

```CMake
if(BUILD_SHARED_LIBS)
  set(HDF5_USE_STATIC_LIBS FALSE)
else()
  set(HDF5_USE_STATIC_LIBS ON)
endif()
+if(VTK_USE_SYSTEM_HDF5)
+  set(vtkhdf5_LIBRARIES ${HDF5_LIBRARIES} ${HDF5_HL_LIBRARIES})
+endif()
vtk_module(vtkhdf5
  DEPENDS
    vtkzlib
  EXCLUDE_FROM_WRAPPING
  )
-if(VTK_USE_SYSTEM_HDF5)
-  set(vtkhdf5_LIBRARIES ${HDF5_LIBRARIES} ${HDF5_HL_LIBRARIES})
-endif()
```

> - extra empty line after last line:
>     + GUISupport/MFC
> - Rendering/OpenGL2  would wrongly include Rendering/OpenGL
>     + vtkWin32RenderWindowInteractor.cxx:#include "vtkTDxWinDevice.h", but it belongs to Rendering/OpenGL
> dirty data after using this script in the first time 
> - add `vtkIOGeometry` to test_depends in following module:
>     + Domains/Chemistry
>     + Domains/ChemistryOpenGL2
>     + filters/General
> - add `vtksqlite` to private_dependency in `IO/SQL`
> - add `vtkRenderFreeType` to public_dependency in Rendering/Label
>     + otherwise the test in RenderingLabel would fail. So you should add the module here (Covered in **addPrivateModuleList**)
> - remove `vtkInfovisBoostGraphAlgorithms` from `vtkInfovisLayout`(VTK_USE_BOOST is obseleted see the email with David Thompson for detail-vtkTimerlogBug) (Covered in **excludePrivateModuleList**)
> - rewrite the `target_link_libraries` in Rendering/OpenGL/Testing/Cxx/CMakeLists.txt 
>     + the older link command is obselete
```text
   target_link_libraries(${exe} LINK_PRIVATE
-    vtkRenderingOpenGL
-    vtkInteractionStyle
-    vtkImagingSources
-    vtkImagingGeneral
-    vtkFiltersSources
-    vtkIOCore)
+    ${${vtk-module-test}-Cxx_LIBRARIES})
```
>   + add `vtkGUISupportQt` to `vtkRenderingQt` private dependency
>   + move `vtkGUISupportQt` and `vtkViewsInfovis` in `vtkViewsQt` module from private to public dependency
>   + remove `vtkRenderingQt` from `vtkViewsInfovis` in private dependency(otherwise you would get REquested modules` not available: vtkViewsQt bug. Since Qt is not enabled, you should have nothing related to Qt)
> - add `vtkIOGeometry` to `Examples/IO/Cxx/CMakeLists.txt` find_package




## File Description 

### UpdateModuleDependency.py (main file)
> - Description: update the private and public dependency in module.cmake file of VTK folder 
> - pseudocode:

```text
function UpdateModuleCMake
Get a list of paths that have module.cmake file(store them in a map[PATH] = module)
Get header files inclusion information in each path. Replace the included header with corresponding path
Get source files inclusion information in each path. Replace the included header with corresponding path 
Create an map to locate the subpath and its corresponding dependency modules(map[path] = publicDependency/privateDependency)
Get the modules in Utlities and ThirdParty (These two modules have unique name rules, so extract them out differently.)
for each subPath:
    Check whether we have corner cases in this path.(public or private modules that should be added extra, 
        since they cannot be found in any headers. Ex.`vtkRenderFreeType` which actually is an interface of `vtkCommonCore`)
    Filter the old public and private dependency for currentPath(Most are OpenGL related module, check the discussion before)
    Get the public and private dependency for the currentPath from the map we created before
    Concatenate cornerCases,oldDependencyFiltered,newDependencyFound into a new list for public and private dependency
    Exclude the modules you don't want.
    write the new module.cmake file
```

### FindModuleDependency.py (helper function file)
> - Description: given a path A, find out all the paths for the headers that the .alpha file inside path A include.(.alpha can be .cxx or .h file), then replace the file with the corresponding module name in module.cmake file.
> - pseudocode:

```text
get input: path, suffix, outputfilename
get all paths and create a map(header->path) and a map (path->module name)
loop through each path
    find out all files end in suffix 
    loop through each file 
        find out it includes which headers 
        loop through each header:
            map header->path->module name 
            store the result
```


### FindPathDependency.py (Ignore it. Idea Validating only)
> - Description: given a path A, find out all the paths for the headers that the .alpha file inside path A include.(.alpha canbe .cxx or .h file)
> - Example:
>     + Common/Core/A.h Common/ExecuteModel/B.h
>     + IO/Geometry/C.h_include_A.h IO/Geometry/D.h_include_(A.h B.h)
>     + result: 
>     IO/Geometry
>       Common/Core
>       Common/ExecuteModel

### test/
> - Manually Created test folder for development only


# Building in PV
## List of bugs on dashboard(all fixed) 
> - CMake Error at VTK/Rendering/OpenGL/CMakeLists.txt:3 (message): vtkRenderingOpenGL cannot be built with vtkRenderingOpenGL2, please disable one of them.
>     + solution: in `vtkRenderingLIC` you cannot have  `vtkRendering${VTK_RENDERING_BACKEND}` and `vtkRendeingOpenGL` at the same time. *Delete the later one*. **fixed in function *CombineDLandDLFiltered* @ UpdateModuleDependency.py**
>     + same in `vtkRenderingLICOpenGL2`
>         * 
> - CMake error: Attempt to add a custom rule to output /Wrapping/Python/vtkStructuredGridLIC2DPython.cxx.rule/" which already has a custom rule"
>     + you have 2 custom rule file to same .cxx -> repeated module inclusion
>     + use `git grep -W vtkStructuredGridLIC2D` to locate the module
>     + solution: remove `vtkRenderingLIC` and `vtkRenderingLICOpenGL2` in `vtkRenderingParallelLIC`
> - ion.cxx.o: In function `vtkMPIMToNSocketConnection::SetupWaitForConnection()':
/home/haochengliu/Kitware/ParaView/paraview/ParaViewCore/ClientServerCore/Core/vtkMPIMToNSocketConnection.cxx:150: undefined reference to `vtkServerSocket::New()'
>      + add corresponding module 

## PV change log:
> VTK folder:
> - remove `vtkRenderingOpenGL` in `Rendering/LIC`
> - remove `vtkRenderingOpenGL2` in `Rendering/LICOpenGL2`
> - remove `vtkRenderingLIC` and `vtkRenderingLICOpenGL2` from *public* in `Rendering/ParallelLIC`
> - remove `vtkFiltersPoints` from *public* and *private* in `vkFiltersCore`(configure error on megas in catalyst-editions. Somehow it introduces new testing failure?)
> - add `vtkInteractionWidgets` to `vtkViewsCore` in *public*(trying to fix the bug on **megas** as No such module: "vtkFiltersModeling". `vtkFiltersModeling` needed by `vtkInteractionWidgets`, which is needed by `vtkViewsCore`. But it means nothing in VTK!!! A weird naghty corner case)
> - NV: remove `vtkIOImage` from *private* in `vtkRenderingVolume`, `vtkRenderingCore` from *private* in `vtkIOGeometry`
>     + that's how to fix `cyclic dependencies` on dashboard. Compare vtkIO and vtkRendering module with master and ur module change, test by deleting one by one.
> Paraview folder:
> - add `vtkCommonSystem`, `vtkIOLegacy`, `vtkCommonCore` to vtkPVClientServerCoreCore(path: ParaViewCore/ClientServerCore/Core/module.cmake)
> - add `vtkCommonComputationalGemometry`, `vtkCommonSystem`, `vtkIOImage`, `vtkParallelMPI`  to `ParaViewCore/VTKExtensions/Rendering/module.cmake` ( To solve *MPIEXEC was empty* configure error on *trey*, `vtkparallelMPI` should be added to if `PARAVIEW_USE_MPI` statement)
> - add `vtkFiltersSources` to `vtkSciberQuest` (Solve fatal error LNK1120. Check nemesis dashboard result)
